package club.tushar.mygallery.utils;

public class Constants{
    public static final int CALL_REQUEST = 1;
    public static final int READ_WRITE_EXTERNAL_STORAGE_REQUEST = 7;
    public static final int RECORD_AUDIO_REQUEST = 8;
    public static final int GET_ACCOUNTS = 9;
    public static final int READ_WRITE_CONTACTS_REQUEST = 10;
    public static final int ACCESS_FINE_LOCATION = 11;
    public static final int RECEIVE_SMS = 12;
}

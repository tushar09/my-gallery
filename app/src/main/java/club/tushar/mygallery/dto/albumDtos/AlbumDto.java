package club.tushar.mygallery.dto.albumDtos;

public class AlbumDto{
    private String url, name;
    private int itemCount;

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getUrl(){
        return url;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public int getItemCount(){
        return itemCount;
    }

    public void setItemCount(int itemCount){
        this.itemCount = itemCount;
    }
}

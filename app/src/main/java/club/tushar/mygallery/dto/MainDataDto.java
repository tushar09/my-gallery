package club.tushar.mygallery.dto;

import java.util.ArrayList;
import java.util.List;

public class MainDataDto{

    List<Data> data = new ArrayList<>();

    public List<Data> getData(){
        return data;
    }

    public void setData(List<Data> data){
        this.data = data;
    }

    public static class Data{
        String title, bucket, path, width, heigh, mime_type;
        long date, size;

        public long getDate(){
            return date;
        }

        public void setDate(long date){
            this.date = date;
        }

        public String getPath(){
            return path;
        }

        public void setPath(String path){
            this.path = path;
        }

        public String getTitle(){
            return title;
        }

        public void setTitle(String title){
            this.title = title;
        }


        public String getBucket(){
            return bucket;
        }

        public void setBucket(String bucket){
            this.bucket = bucket;
        }

        public String getWidth(){
            return width;
        }

        public void setWidth(String width){
            this.width = width;
        }

        public String getHeigh(){
            return heigh;
        }

        public void setHeigh(String heigh){
            this.heigh = heigh;
        }

        public String getMime_type(){
            return mime_type;
        }

        public void setMime_type(String mime_type){
            this.mime_type = mime_type;
        }

        public long getSize(){
            return size;
        }

        public void setSize(long size){
            this.size = size;
        }
    }

}

package club.tushar.mygallery.dto;

import java.util.List;

public class ListDataDto{
    List<String> sections;
    List<MainDataDto> datas;

    public List<String> getSections(){
        return sections;
    }

    public void setSections(List<String> sections){
        this.sections = sections;
    }

    public List<MainDataDto> getDatas(){
        return datas;
    }

    public void setDatas(List<MainDataDto> datas){
        this.datas = datas;
    }
}

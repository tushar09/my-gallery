package club.tushar.mygallery.adapters;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;

import java.util.List;

import club.tushar.mygallery.R;
import club.tushar.mygallery.activity.ImageActivity;
import club.tushar.mygallery.databinding.RowGridHeaderBinding;
import club.tushar.mygallery.databinding.RowHomeBinding;
import club.tushar.mygallery.dto.MainDataDto;
import club.tushar.mygallery.dto.timeDtos.DaysDtos;
import club.tushar.mygallery.utils.Constants;

public class HomeAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter{

    private Context context;
    private MainDataDto dto;
    private List<DaysDtos> sections;

    public HomeAdapter(Context context, MainDataDto dto, List<DaysDtos> sections){
        this.context = context;
        this.dto = dto;
        this.sections = sections;
    }

    @Override
    public int getCount(){
        return dto.getData().size();
    }

    @Override
    public Object getItem(int i){
        return dto.getData().get(i);
    }

    @Override
    public long getItemId(int i){
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup){

        Holder holder;

        if(view == null){
            holder = new Holder(context);
            view = holder.binding.getRoot();
            view.setTag(holder);
        }else {
            holder = (Holder) view.getTag();
        }

        Glide.with(context).load(dto.getData().get(position).getPath()).into(holder.binding.ivPic);

        view.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                context.startActivity(new Intent(context, ImageActivity.class)
                        .putExtra("data", new Gson().toJson(dto))
                        .putExtra("index", position)
                );
            }
        });

        return view;
    }

    @Override
    public int getCountForHeader(int i){
        return sections.get(i).getCount();
    }

    @Override
    public int getNumHeaders(){
        return sections.size();
    }

    @Override
    public View getHeaderView(int i, View view, ViewGroup viewGroup){

        HeaderHolder headerHolder;
        if(view == null){
            headerHolder = new HeaderHolder(context);
            view = headerHolder.binding.getRoot();
            view.setTag(headerHolder);
        }else {
            headerHolder = (HeaderHolder) view.getTag();
        }

        headerHolder.binding.tvDate.setText(sections.get(i).getDate());
        headerHolder.binding.tvItemCount.setText(sections.get(i).getCount() + " items");
        return view;
    }

    private class Holder{
        RowHomeBinding binding;
        public Holder(Context context){
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_home, null, true);
        }
    }

    private class HeaderHolder{
        RowGridHeaderBinding binding;
        public HeaderHolder(Context context){
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_grid_header, null, true);
        }
    }

}

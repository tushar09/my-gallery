package club.tushar.mygallery.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.bumptech.glide.Glide;

import java.util.List;

import club.tushar.mygallery.R;
import club.tushar.mygallery.dto.albumDtos.AlbumDto;
import club.tushar.mygallery.databinding.RowAlbumBinding;

public class AlbumAdapter extends BaseAdapter{

    private Context context;
    private List<AlbumDto> dtos;

    public AlbumAdapter(Context context, List<AlbumDto> dtos){
        this.context = context;
        this.dtos = dtos;
    }

    @Override
    public int getCount(){
        return dtos.size();
    }

    @Override
    public Object getItem(int i){
        return null;
    }

    @Override
    public long getItemId(int i){
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup){

        Holder holder;

        if(view == null){
            holder = new Holder(context);
            view = holder.binding.getRoot();
            view.setTag(holder);
        }else {
            holder = (Holder) view.getTag();
        }

        holder.binding.tvCount.setText(dtos.get(i).getItemCount() + " items");
        holder.binding.tvName.setText(dtos.get(i).getName());
        Glide.with(context).load(dtos.get(i).getUrl()).into(holder.binding.ivImage);


        return view;
    }

    private class Holder{
        RowAlbumBinding binding;
        public Holder(Context context){
            binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.row_album, null, true);
        }
    }
}

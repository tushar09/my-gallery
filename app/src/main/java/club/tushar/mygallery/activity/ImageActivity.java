package club.tushar.mygallery.activity;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import club.tushar.mygallery.R;
import club.tushar.mygallery.databinding.ActivityImageBinding;
import club.tushar.mygallery.dto.MainDataDto;

public class ImageActivity extends AppCompatActivity{

    private ActivityImageBinding binding;

    private MainDataDto dto;

    private int index;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_image);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getWindow().setStatusBarColor(Color.parseColor("#00000000"));
        }

        dto = new Gson().fromJson(getIntent().getStringExtra("data"), MainDataDto.class);
        index = getIntent().getIntExtra("index", 0);

        initViewPager();

    }


    private void initViewPager() {

        PagerAdapter adapter = new PagerAdapter() {

            @Override
            public boolean isViewFromObject(View arg0, Object arg1) {
                return arg0 == arg1;
            }

            @Override
            public void destroyItem(ViewGroup container, int position, Object obj) {
                container.removeView((View) obj);
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                View view = View.inflate(container.getContext(), R.layout.pager_item, null);
                ImageView imageView = (ImageView) view.findViewById(R.id.iv_image);

                Glide.with(ImageActivity.this).load(dto.getData().get(position).getPath()).into(imageView);
                //imageView.setImage(ImageSource.uri(dto.getData().get(position).getPath()));
                container.addView(view, ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT);
                imageView.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View view){
                        int flags = getWindow().getAttributes().flags;
                        if ((flags & WindowManager.LayoutParams.FLAG_FULLSCREEN) != 0) {
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                            binding.container.slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                        } else {
                            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
                            binding.container.slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                        }
                    }
                });
                return view;
            }

            @Override
            public int getCount() {
                return dto.getData().size();
            }

        };
        binding.container.viewpager.setAdapter(adapter);
        binding.container.viewpager.setCurrentItem(index);
        binding.container.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener(){
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels){

            }

            @Override
            public void onPageSelected(int position){
                binding.container.imageDetailsContainer.tvName.setText("Name: " + dto.getData().get(position).getTitle());
                binding.container.imageDetailsContainer.tvAlbum.setText("Album: " + dto.getData().get(position).getBucket());
                binding.container.imageDetailsContainer.tvResulation.setText("Resulation: " + dto.getData().get(position).getWidth() + "X" + dto.getData().get(position).getHeigh());
                binding.container.imageDetailsContainer.tvDate.setText("Date: " + dto.getData().get(position).getDate());
            }

            @Override
            public void onPageScrollStateChanged(int state){

            }
        });
    }

}

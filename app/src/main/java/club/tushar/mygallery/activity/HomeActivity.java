package club.tushar.mygallery.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.aurelhubert.ahbottomnavigation.notification.AHNotification;
import com.google.gson.Gson;
import com.otaliastudios.cameraview.WhiteBalance;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import club.tushar.mygallery.R;
import club.tushar.mygallery.adapters.AlbumAdapter;
import club.tushar.mygallery.adapters.HomeAdapter;
import club.tushar.mygallery.databinding.ActivityHomeBinding;
import club.tushar.mygallery.dto.MainDataDto;
import club.tushar.mygallery.dto.albumDtos.AlbumDto;
import club.tushar.mygallery.dto.timeDtos.DaysDtos;

public class HomeActivity extends AppCompatActivity{

    private ActivityHomeBinding binding;

    private MainDataDto mdd, mddCamera;

    private HomeAdapter adapter;

    private SimpleDateFormat sdf;

    private List<DaysDtos> sections;

    private List<AlbumDto> albums;

    private int[] tabColors;
    private AHBottomNavigationAdapter navigationAdapter;

    private Map<String, AlbumDto> albumDtos;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);


        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
            getWindow().setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_1_transparent)));
        }


        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);


        albums = new ArrayList<>();

        albumDtos = new HashMap<>();

        sdf = new SimpleDateFormat("dd MMM EEE");

        mdd = new Gson().fromJson(getIntent().getStringExtra("data"), MainDataDto.class);
        mddCamera = new Gson().fromJson(getIntent().getStringExtra("dataCamera"), MainDataDto.class);

        loadCameraPictures();
        sections = new ArrayList<>();

        long current = Calendar.getInstance().getTimeInMillis();

        String currentDateState = "";
        int currentDateCount = 0;
        for(int t = 0; t < mdd.getData().size(); t++){
            String s = getDateDifferences(current - mdd.getData().get(t).getDate());
            if(currentDateState.equals(s)){
                currentDateCount++;
            }else{
                if(t == 0){
                    currentDateState = s;
                    currentDateCount++;
                }else{
                    DaysDtos d = new DaysDtos();
                    d.setCount(currentDateCount);
                    d.setDate(currentDateState);

                    sections.add(d);

                    currentDateState = s;
                    currentDateCount = 1;

                }

            }

            //AlbumDto album = albumDtos.get();
            if(albumDtos.containsKey(mdd.getData().get(t).getBucket())){
                AlbumDto album = albumDtos.get(mdd.getData().get(t).getBucket());
                album.setItemCount(album.getItemCount() + 1);
                //album.setUrl(mdd.getData().get(t).getPath());
            }else {
                AlbumDto album = new AlbumDto();
                album.setItemCount(1);
                album.setUrl(mdd.getData().get(t).getPath());
                album.setName(mdd.getData().get(t).getBucket());
                albumDtos.put(mdd.getData().get(t).getBucket(), album);
            }

            //Log.e("date", s);
        }

        Collection c = albumDtos.values();

        albums = new ArrayList<>(c);
        Collections.sort(albums, new Comparator<AlbumDto>(){
            @Override
            public int compare(AlbumDto albumDto, AlbumDto t1){
                return t1.getItemCount() - albumDto.getItemCount() ;
            }
        });
        final AlbumAdapter albumAdapter = new AlbumAdapter(this, albums);
        binding.container.gvAlbums.setAdapter(albumAdapter);

        List<MainDataDto> mdds = new ArrayList<>();

        adapter = new HomeAdapter(this, mdd, sections);

        binding.container.gvImages.setAdapter(adapter);

        tabColors = getApplicationContext().getResources().getIntArray(R.array.tab_colors);
        navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_navigation_menu_3);
        binding.container.bottomNavigation.manageFloatingActionButtonBehavior(binding.fab);
        binding.container.bottomNavigation.setTranslucentNavigationEnabled(true);

        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.menu_1, R.drawable.ic_image_black_24dp, R.color.color_tab_1);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.menu_2, R.drawable.ic_photo_album_black_24dp, R.color.color_tab_2);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.menu_3, R.drawable.database, R.color.color_tab_3);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.menu_4, R.drawable.ic_camera_alt_black_24dp, R.color.color_tab_4);
        AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.menu_5, R.drawable.file_tree, R.color.color_tab_5);

        binding.container.bottomNavigation.addItem(item1);
        binding.container.bottomNavigation.addItem(item2);
        binding.container.bottomNavigation.addItem(item3);
        binding.container.bottomNavigation.addItem(item4);
        binding.container.bottomNavigation.addItem(item5);
        binding.container.bottomNavigation.setAccentColor(Color.parseColor("#fda129"));
        binding.container.bottomNavigation.setInactiveColor(Color.parseColor("#02acf3"));
        binding.container.bottomNavigation.setTitleState(AHBottomNavigation.TitleState.SHOW_WHEN_ACTIVE);
        binding.container.bottomNavigation.setColored(true);
        binding.container.bottomNavigation.setForceTint(true);


        binding.container.bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener(){
            @Override
            public boolean onTabSelected(int y, boolean wasSelected){
                if(y == 0){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                        getWindow().setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_1_transparent)));
                        //setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_1_transparent)));
                    }
                    binding.container.gvCamera.setVisibility(View.VISIBLE);
                    binding.container.llAlbum.setVisibility(View.GONE);
                    binding.container.gvImages.setVisibility(View.GONE);
                    //binding.container.camera.setVisibility(View.GONE);
                }else if(y == 1){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                        getWindow().setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_2_transparent)));
                        //setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_2_transparent)));
                    }
                    binding.container.gvImages.setVisibility(View.GONE);
                    binding.container.llAlbum.setVisibility(View.VISIBLE);
                    binding.container.gvCamera.setVisibility(View.GONE);
                    //binding.container.camera.setVisibility(View.GONE);
                }else if(y == 2){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                        getWindow().setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_3_transparent)));
                        //setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_2_transparent)));
                    }
                    binding.container.gvCamera.setVisibility(View.GONE);
                    binding.container.llAlbum.setVisibility(View.GONE);
                    binding.container.gvImages.setVisibility(View.VISIBLE);
                    //binding.container.camera.setVisibility(View.GONE);
                }else if(y == 3){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                        getWindow().setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_4_transparent)));
                        //setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_2_transparent)));
                    }
                    //binding.container.gvCamera.setVisibility(View.GONE);
                    //binding.container.gvImages.setVisibility(View.GONE);
                    //binding.container.llAlbum.setVisibility(View.GONE);
                    //binding.container.camera.setVisibility(View.VISIBLE);
                    Intent intent = new Intent(MediaStore.INTENT_ACTION_STILL_IMAGE_CAMERA);
                    startActivity(intent);
                    return false;
                }else if(y == 4){
                    if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
                        getWindow().setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_5_transparent)));
                        //setStatusBarColor(Color.parseColor(getResources().getString(R.color.color_tab_2_transparent)));
                    }
                    binding.container.gvImages.setVisibility(View.GONE);
                    binding.container.llAlbum.setVisibility(View.VISIBLE);
                }
                Log.e("position tab", y + "");
                return true;
            }
        });

    }

    private void loadCameraPictures(){
        ArrayList<DaysDtos> sectionsCamera = new ArrayList<>();

        long current = Calendar.getInstance().getTimeInMillis();

        String currentDateState = "";
        int currentDateCount = 0;
        for(int t = 0; t < mddCamera.getData().size(); t++){
            String s = getDateDifferences(current - mddCamera.getData().get(t).getDate());
            if(currentDateState.equals(s)){
                currentDateCount++;
            }else{
                if(t == 0){
                    currentDateState = s;
                    currentDateCount++;
                }else{
                    DaysDtos d = new DaysDtos();
                    d.setCount(currentDateCount);
                    d.setDate(currentDateState);

                    sectionsCamera.add(d);

                    currentDateState = s;
                    currentDateCount = 1;

                }

            }

        }

        HomeAdapter adapterCamera = new HomeAdapter(this, mddCamera, sectionsCamera);

        binding.container.gvCamera.setAdapter(adapterCamera);

    }


    private String getDateDifferences(long l){
        long days = TimeUnit.MILLISECONDS.toDays(l);
        if(days < 2){
            if(days == 1){
                return days + " days ago";
            }else{
                return "Today";
            }
        }else{
            return sdf.format(new Date(l));
        }
    }


//    @Override
//    protected void onResume() {
//        super.onResume();
//        binding.container.camera.start();
//        WhiteBalance whiteBalance = binding.container.camera.getWhiteBalance();
//
//        binding.container.camera.setWhiteBalance();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        binding.container.camera.stop();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        binding.container.camera.destroy();
//    }

}

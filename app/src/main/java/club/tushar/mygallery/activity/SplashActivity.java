package club.tushar.mygallery.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MergeCursor;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;
import com.google.gson.Gson;

import java.security.Permission;
import java.util.ArrayList;
import java.util.List;

import club.tushar.mygallery.R;
import club.tushar.mygallery.databinding.ActivitySplashBinding;
import club.tushar.mygallery.dto.MainDataDto;
import club.tushar.mygallery.utils.CheckPermission;
import club.tushar.mygallery.utils.Constants;
import io.fabric.sdk.android.Fabric;

public class SplashActivity extends AppCompatActivity{

    private ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Fabric.with(this, new Crashlytics());
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        if(CheckPermission.readAndWriteExternalStorage(this)){
            loadData();
        }
    }

    private void loadData(){

        String orderBy = MediaStore.Images.Media.DATE_TAKEN + " DESC";

        String[] projection = new String[] {
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.DATE_TAKEN,
                MediaStore.Images.Media.TITLE,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.WIDTH,
                MediaStore.Images.Media.HEIGHT,
                MediaStore.Images.Media.MIME_TYPE
        };

        // content:// style URI for the "primary" external storage volume
        Uri images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        // Make the query.
        Cursor cur = managedQuery(images,
                projection, // Which columns to return
                null,       // Which rows to return (all rows)
                null,       // Selection arguments (none)
                orderBy        // Ordering
        );


        String[] projection2 = new String[] {
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.DATE_TAKEN,
                MediaStore.Images.Media.TITLE,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.WIDTH,
                MediaStore.Images.Media.HEIGHT,
                MediaStore.Images.Media.MIME_TYPE
        };

        // content:// style URI for the "primary" external storage volume
        Uri images2 = MediaStore.Images.Media.INTERNAL_CONTENT_URI;

        // Make the query.
        Cursor cur2 = managedQuery(images2,
                projection2, // Which columns to return
                null,       // Which rows to return (all rows)
                null,       // Selection arguments (none)
                orderBy        // Ordering
        );

        MergeCursor mergedCursor = new MergeCursor(new Cursor[]{cur, cur2});
        new DataLoader().execute(mergedCursor);

    }

    private class DataLoader extends AsyncTask<MergeCursor, Integer, String[]>{

        @Override
        protected String[] doInBackground(MergeCursor... mergeCursors){

            Cursor cur = mergeCursors[0];
            int count = cur.getCount();
            int total = 0;
            long wait = 500 / count;
            Log.e("time", wait + "");

            MainDataDto mdd = new MainDataDto();
            MainDataDto mddCamera = new MainDataDto();
            List<MainDataDto.Data> dataList = new ArrayList<>();
            List<MainDataDto.Data> dataListCamera = new ArrayList<>();

            if (cur.moveToFirst()) {
                String bucket;
                long date, size;
                String title;
                String path;
                String width, height, mime_type;

                int bucketColumn = cur.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME);
                int dateColumn = cur.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN);
                int titleColumn = cur.getColumnIndex(MediaStore.Images.Media.TITLE);
                int pathColumn = cur.getColumnIndex(MediaStore.Images.Media.DATA);
                int sizeColumn = cur.getColumnIndex(MediaStore.Images.Media.SIZE);
                int widthColumn = cur.getColumnIndex(MediaStore.Images.Media.WIDTH);
                int heightColumn = cur.getColumnIndex(MediaStore.Images.Media.HEIGHT);
                int mime_typeColumn = cur.getColumnIndex(MediaStore.Images.Media.MIME_TYPE);

                do {
                    total++;
                    // Get the field values
                    bucket = cur.getString(bucketColumn);
                    date = cur.getLong(dateColumn);
                    title = cur.getString(titleColumn);
                    path = cur.getString(pathColumn);
                    size = cur.getLong(sizeColumn );
                    width = cur.getString(widthColumn );
                    height = cur.getString(heightColumn );
                    mime_type = cur.getString(mime_typeColumn) ;

                    MainDataDto.Data data = new MainDataDto.Data();
                    data.setBucket(bucket);
                    data.setTitle(title);
                    data.setDate(date);
                    data.setPath(path);
                    data.setSize(size );
                    data.setWidth(width );
                    data.setHeigh(height );
                    data.setMime_type(mime_type );

                    dataList.add(data);

                    if(bucket.equalsIgnoreCase("camera")){
                        MainDataDto.Data dataCamera = new MainDataDto.Data();
                        dataCamera.setBucket(bucket);
                        dataCamera.setTitle(title);
                        dataCamera.setDate(date);
                        dataCamera.setPath(path);
                        data.setSize(size );
                        data.setWidth(width );
                        data.setHeigh(height );
                        data.setMime_type(mime_type );

                        dataListCamera.add(dataCamera);
                    }


                    publishProgress((int) (total * 100 / count));


                } while (cur.moveToNext());

            }
            mdd.setData(dataList);
            mddCamera.setData(dataListCamera);
            return new String []{new Gson().toJson(mdd), new Gson().toJson(mddCamera)};
        }

        @Override
        protected void onProgressUpdate(Integer... values){
            super.onProgressUpdate(values);

            binding.container.pbProgress.setProgress(values[0]);

        }

        @Override
        protected void onPostExecute(String [] s){
            super.onPostExecute(s);
            startActivity(new Intent(SplashActivity.this, HomeActivity.class)
                    .putExtra("data", s[0])
                    .putExtra("dataCamera", s[1])
            );
            finish();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults){
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == Constants.READ_WRITE_EXTERNAL_STORAGE_REQUEST){
            if(permissions.length == 2 && grantResults.length == 2){
                if(grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED){
                    loadData();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if(id == R.id.action_settings){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}